# SPO task

### Tools
Java 8, SpringFramework, JUnit4,  Maven.

## How it works
A controller is listening for requests to path **/sanitation** and accepts a JSON request that has three variables:
An array of integers with key named **rooms**,
An integer named **seniors**,
and another integer named **juniors**.

Request Path: http://localhost:8080/sanitation
JSON data example: 

    {
      "rooms" = [35, 21, 17],
      "senior" = 10,
      "junior" = 6
    }

The response is again JSON data that contains an array of CleaningGroups objects. A group contains the number of seniors and the number of juniors needed for a structure of rooms.
Response Json data example:

    {
      [ 
        {senior: 3, junior: 1}, 
        {senior: 1, junior: 2},
	    {senior: 2, junior: 0} 
	  ]
    }

## Code explained 

### The most important  method

domain.CleaningGroups.createGroup(int rooms).  

(note: the method its not static)

This method is the part of the  algorithm that creates the groups.
Takes a the number of rooms for a structure and add a group to the array of cleaning groups.

This algorithm makes sure that the chosen group has the lowest **idle capacity**.
> **Idle capacity example.**
>
>rooms = 14
>
> senior = 10
>
> junior = 5
>
> If we use 2 seniors for the structore the idle capacity will be 2x10-14 = 6
>
> If we use 1 senior and 1 junior for the structure the idle capacity will be 1x10+1x6-14 = 2
>
>2<6,  the algorithm will chose 1 junior and 1 senior for the structure. 

### Maximum Seniors for a structure
max = rooms / SeniorCapacity

 ### Maximum Juniors for a structure 
 max = 2xSeniorCapacity / JuniorCapacity 

## Issues 

The task was somehow ambiguous, did i have to prioritize seniors over juniors or the other way around? The number of juniors and seniors available were not declared. That's why i set a system that the algorithm makes sure that the chosen group has the lowest **idle capacity** and that the maximum of seniors and juniors possible are the one mentioned above.
package spo.domains;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CleaningGroupsUT {

  @Test
  public void testSceneOne() {
    // Given
    int[] rooms = {35, 21, 17};
    int senior = 10;
    int junior = 6;
    WOCleaningRequest request = new WOCleaningRequest(rooms, senior, junior);


    // When
    CleaningGroups response = new CleaningGroups(request);
    response.createCleaningGroups();

    // Then
    CleaningGroup[] cleaningGroups = response.getCleaningGroups();
    Assert.assertFalse(response.isCollapsed());
    Assert.assertEquals(cleaningGroups.length, 3);
    Assert.assertEquals(cleaningGroups[0].getSeniors(), 3);
    Assert.assertEquals(cleaningGroups[0].getJuniors(), 1);
    Assert.assertEquals(cleaningGroups[1].getSeniors(), 1);
    Assert.assertEquals(cleaningGroups[1].getJuniors(), 2);
    Assert.assertEquals(cleaningGroups[2].getSeniors(), 2);
    Assert.assertEquals(cleaningGroups[2].getJuniors(), 0);
  }

  @Test
  public void testSceneTwo() {
    // Given
    int[] rooms = {24, 28};
    int senior = 11;
    int junior = 6;
    WOCleaningRequest request = new WOCleaningRequest(rooms, senior, junior);


    // When
    CleaningGroups response = new CleaningGroups(request);
    response.createCleaningGroups();

    // Then
    CleaningGroup[] cleaningGroups = response.getCleaningGroups();
    Assert.assertFalse(response.isCollapsed());
    Assert.assertEquals(cleaningGroups.length, 2);
    Assert.assertEquals(cleaningGroups[0].getSeniors(), 2);
    Assert.assertEquals(cleaningGroups[0].getJuniors(), 1);
    Assert.assertEquals(cleaningGroups[1].getSeniors(), 2);
    Assert.assertEquals(cleaningGroups[1].getJuniors(), 1);
  }

  @Test
  public void testSceneAdditional() {
    // Given
    int[] rooms = {1, 6, 10 , 12, 16, 19, 20, 22, 28};
    int senior = 10;
    int junior = 6;
    WOCleaningRequest request = new WOCleaningRequest(rooms, senior, junior);


    // When
    CleaningGroups response = new CleaningGroups(request);
    response.createCleaningGroups();

    // Then
    CleaningGroup[] cleaningGroups = response.getCleaningGroups();
    Assert.assertFalse(response.isCollapsed());
    Assert.assertEquals(cleaningGroups.length, 9);
    Assert.assertEquals(cleaningGroups[0].getSeniors(), 1);
    Assert.assertEquals(cleaningGroups[0].getJuniors(), 0);
    Assert.assertEquals(cleaningGroups[1].getSeniors(), 1);
    Assert.assertEquals(cleaningGroups[1].getJuniors(), 0);
    Assert.assertEquals(cleaningGroups[2].getSeniors(), 1);
    Assert.assertEquals(cleaningGroups[2].getJuniors(), 0);
    Assert.assertEquals(cleaningGroups[3].getSeniors(), 1);
    Assert.assertEquals(cleaningGroups[3].getJuniors(), 1);
    Assert.assertEquals(cleaningGroups[4].getSeniors(), 1);
    Assert.assertEquals(cleaningGroups[4].getJuniors(), 1);
    Assert.assertEquals(cleaningGroups[5].getSeniors(), 2);
    Assert.assertEquals(cleaningGroups[5].getJuniors(), 0);
    Assert.assertEquals(cleaningGroups[6].getSeniors(), 2);
    Assert.assertEquals(cleaningGroups[6].getJuniors(), 0);
    Assert.assertEquals(cleaningGroups[7].getSeniors(), 1);
    Assert.assertEquals(cleaningGroups[7].getJuniors(), 2);
    Assert.assertEquals(cleaningGroups[8].getSeniors(), 1);
    Assert.assertEquals(cleaningGroups[8].getJuniors(), 3);
  }

  @Test
  public void testStructuresOutOfBoundaries(){
    // Given
    int[] rooms = {-1, 101};
    int senior = 10;
    int junior = 6;
    WOCleaningRequest request = new WOCleaningRequest(rooms, senior, junior);


    // When
    CleaningGroups response = new CleaningGroups(request);
    response.createCleaningGroups();

    // Then
    CleaningGroup[] cleaningGroups = response.getCleaningGroups();
    Assert.assertFalse(response.isCollapsed());
    Assert.assertEquals(cleaningGroups.length, 2);
    Assert.assertEquals(cleaningGroups[0].getSeniors(), -1);
    Assert.assertEquals(cleaningGroups[0].getJuniors(), -1);
    Assert.assertEquals(cleaningGroups[1].getSeniors(), -1);
    Assert.assertEquals(cleaningGroups[1].getJuniors(), -1);
  }

  @Test
  public void testSeniorCapacityIsZero() {
    // Given
    int[] rooms = new int[10];
    int senior = 0;
    int junior = 6;
    WOCleaningRequest request = new WOCleaningRequest(rooms, senior, junior);
    Assert.assertTrue(request.isCollapsed());
  }

  @Test
  public void testJuniorCapacityIsZero() {
    // Given
    int[] rooms = new int[10];
    int senior = 7;
    int junior = 0;
    WOCleaningRequest request = new WOCleaningRequest(rooms, senior, junior);
    Assert.assertTrue(request.isCollapsed());
  }

  @Test
  public void testNrOfRoomsOutOfBound() {
    // Given
    int[] rooms = new int[110];
    int senior = 7;
    int junior = 6;
    WOCleaningRequest request = new WOCleaningRequest(rooms, senior, junior);
    Assert.assertTrue(request.isCollapsed());
  }

  @Test
  public void testStructuresZero() {
    // Given
    int[] rooms = {0, 0, 0};
    int senior = 10;
    int junior = 6;
    WOCleaningRequest request = new WOCleaningRequest(rooms, senior, junior);

    // When
    CleaningGroups response = new CleaningGroups(request);
    response.createCleaningGroups();

    // Then
    CleaningGroup[] cleaningGroups = response.getCleaningGroups();
    Assert.assertFalse(response.isCollapsed());
    Assert.assertEquals(cleaningGroups.length, 3);
    Assert.assertEquals(cleaningGroups[0].getSeniors(), 0);
    Assert.assertEquals(cleaningGroups[0].getJuniors(), 0);
    Assert.assertEquals(cleaningGroups[1].getSeniors(), 0);
    Assert.assertEquals(cleaningGroups[1].getJuniors(), 0);
    Assert.assertEquals(cleaningGroups[2].getSeniors(), 0);
    Assert.assertEquals(cleaningGroups[2].getJuniors(), 0);

  }

  @Test
  public void testNoRooms() {
    int[] rooms = {};
    int senior = 10;
    int junior = 6;
    WOCleaningRequest request = new WOCleaningRequest(rooms, senior, junior);


    // When
    CleaningGroups response = new CleaningGroups(request);
    response.createCleaningGroups();

    // Then
    CleaningGroup[] cleaningGroups = response.getCleaningGroups();
    Assert.assertFalse(response.isCollapsed());
    Assert.assertEquals(cleaningGroups.length, 0);
  }

}
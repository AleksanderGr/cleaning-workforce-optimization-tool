package spo.domains;

/**
 * This class create the instance of a cleaning request
 */
public class WOCleaningRequest {

  protected static final int MAX_STRUCTURES = 100;

  private int senior;
  private int junior;
  private boolean collapsed;
  private int[] rooms = new int[MAX_STRUCTURES];

  //constructors
  public WOCleaningRequest() {
    this.senior = 1;
    this.junior = 1;
  }

  public WOCleaningRequest(int[] rooms, int senior, int junior) {
    this.collapsed = false;
    try {
      if (rooms.length > MAX_STRUCTURES)
        throw new ArrayIndexOutOfBoundsException("Array has more than the allowed maximum of elements");
      if (senior == 0 || junior == 0)
        throw new Exception("Senior Capacity or Junior Capacity has value of 0");

      this.rooms = rooms;
      this.senior = senior;
      this.junior = junior;

    } catch (Exception error) {
      error.printStackTrace();
      this.collapsed = true;
    }
  }

  //methods getters and setters
  public int[] getRooms() {
    return rooms;
  }

  public int getSenior() {
    return senior;
  }

  public int getJunior() {
    return junior;
  }

  public static int getMaxrooms() {
    return MAX_STRUCTURES;
  }

  public boolean isCollapsed() {
    return collapsed;
  }
}

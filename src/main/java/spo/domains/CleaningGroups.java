package spo.domains;

import java.util.Arrays;

/**
 * This class creates an Object that reads the request
 * from the user and can return an array of CleaningGroups
 *
 */
public class CleaningGroups extends WOCleaningRequest {
  //Fields
  private CleaningGroup[] groups = new CleaningGroup[MAX_STRUCTURES];
  private int groupSize;

  public CleaningGroups() {
    // Constructor
  }

  public CleaningGroups(int[] rooms, int seniorCap, int juniorCap) {
    super(rooms, seniorCap, juniorCap);
    groupSize = 0;
  }

  public CleaningGroups(WOCleaningRequest request) {
    this(request.getRooms(), request.getSenior(), request.getJunior());
  }

  /**
   * This is the CORE public method of the application. This method
   * calculate and put CleaningGroup-s in groups[] array
   */
  public void createCleaningGroups() {
    CleaningGroup newGroup;
    for (int rooms : this.getRooms()) {
      if (rooms <= 100 && rooms > 0)
        newGroup = createGroup(rooms);
      else if(rooms==0)
        newGroup = new CleaningGroup(0, 0);
      else
        newGroup = new CleaningGroup(-1, -1);
      this.addGroup(newGroup);
    }
  }

  /**
   * Gets Cleaning Groups
   *
   * @return Array of CleaningGroup
   */
  public CleaningGroup[] getCleaningGroups() {
    return Arrays.copyOfRange(groups, 0, groupSize);
  }

  /**
   * This function create a Cleaning Group.
   *
   * @param rooms is the number of rooms for a given rooms
   * @return the ideal CleaningGroup object
   */
  private CleaningGroup createGroup(int rooms) {
    int seniors = rooms / getSenior();
    int juniors = 0;
    int unassignedRooms = rooms % getSenior();
    if (getJunior() != unassignedRooms && unassignedRooms != 0 && seniors != 0) {
      if (seniors > 1 && !isBetterMaxSeniors(unassignedRooms)) {
        seniors--;
        unassignedRooms += getSenior();
      }
      if (unassignedRooms%getJunior() == 0)
        juniors = unassignedRooms / getJunior();
      else if (seniorIsBestFitFor(unassignedRooms))
        seniors++;
      else {
        juniors = unassignedRooms / getJunior();
        if (unassignedRooms % getJunior() != 0)
          juniors++;
      }
    } else if (seniors == 0){
      seniors++;
    }
      else if (unassignedRooms != 0)
      juniors++;
    return new CleaningGroup(seniors, juniors);
  }

  /**
   * This function calculates the minimum idle capacity of the team when is used the maximum possible number of seniors
   * and the minimum idle capacity of the team when is not used the maximum possible number of seniors, which means one
   * senio less.
   *
   * @param rooms is the unassigned rooms of the structure
   * @return if the minimum idle capacity of the team is when we engage the maximum possible nr of seniors or not
   */
  private boolean isBetterMaxSeniors(int rooms) {
    return getIdleCapacity(rooms) < getIdleCapacity(rooms + getSenior());
  }

  /**
   * This functions calculates the idle capacity of the team in both cases if we use a senior or juniors for the
   * unassigned rooms.
   *
   * @param rooms is the number of unsigned rooms
   * @return an integer that represents the lowest idle capacity between the senior and junior
   */
  private int getIdleCapacity(int rooms) {
    int idleCapacityUsingJuniors = 0 ;
    int idleCapacityusingSeniors = 0;

    if (rooms%getJunior() != 0)
      idleCapacityUsingJuniors = getJunior() - rooms % getJunior();
    if (rooms%getSenior() != 0)
      idleCapacityusingSeniors = getSenior() - rooms % getSenior();

    if (idleCapacityUsingJuniors < idleCapacityusingSeniors)
      return idleCapacityUsingJuniors;
    else
      return idleCapacityusingSeniors;
  }

  /**
   * the same as getIdleCapacity(int)
   *
   * @param rooms is the number of unsigned rooms
   * @return if a senior is  better fitted for the position ( more efficient)
   */
  private boolean seniorIsBestFitFor(int rooms) {
    int idleCapacityUsingJuniors = getJunior() - rooms % getJunior();
    int idleCapacityusingSeniors = getSenior() - rooms % getSenior();
    return idleCapacityUsingJuniors > idleCapacityusingSeniors;
  }

  private void addGroup(CleaningGroup group) {
    this.groups[groupSize++] = group;
  }

}

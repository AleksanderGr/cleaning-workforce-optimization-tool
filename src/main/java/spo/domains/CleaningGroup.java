package spo.domains;

/**
 * This Class creates a instance of a Cleaning group (seniors and juniors)
 */
public class CleaningGroup {

  //fields
  private int seniors;
  private int juniors;

  //constructors
  public CleaningGroup(int seniors, int juniors) {
    this.seniors = seniors;
    this.juniors = juniors;
  }

  public CleaningGroup() {
    this(0, 0);
  }

  //methods getters and setters
  public int getSeniors() {
    return seniors;
  }

  public void setSeniors(int seniors) {
    this.seniors = seniors;
  }

  public int getJuniors() {
    return juniors;
  }

  public void setJuniors(int juniors) {
    this.juniors = juniors;
  }
}

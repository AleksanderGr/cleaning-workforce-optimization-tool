package spo.controllers;

import spo.domains.CleaningGroup;
import spo.domains.CleaningGroups;

import spo.domains.WOCleaningRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Workforce optimisation (WO) Cleaning Controller
 * This Class is a controller for requests from front-end
 */

@RestController
public class WOCleaningController {

  /**
   * This function waits for a request from the user to set up groups for clening given structures
   * @param request the JSON request transformed to WOCleaningRequest object
   * @return a ResponseEntity with a BAD_REQUEST or OK status and an array of CleaningGroups
   */
  @PostMapping("/sanitation")
  public ResponseEntity<CleaningGroup[]> createGroups(@RequestBody WOCleaningRequest request) {
    CleaningGroups response = new CleaningGroups(request);
    if (response.isCollapsed()) {
      return new ResponseEntity<CleaningGroup[]>(response.getCleaningGroups(), HttpStatus.BAD_REQUEST);
    }
    response.createCleaningGroups();
    return new ResponseEntity<CleaningGroup[]>(response.getCleaningGroups(), HttpStatus.OK);
  }
}
